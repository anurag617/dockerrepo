FROM node:9-alpine

COPY . /src
RUN cd /src && npm \
install
EXPOSE \
80 90 60
CMD ["node", "/src/server.js"]
